import './User.css';
import { useEffect, useReducer, useState } from 'react';

const initialState = {
  firstName: '',
  lastName: '',
  age: '',
  employed: false,
  color: '',
  sauce: {
    ketchup: false,
    mustard: false,
    mayonnaise: false,
    guacamole: false,
  },
  stooge: 'Larry',
  notes: ''
};

function reducer(state, action) {
  switch (action.type) {
    case 'firstName':
      return { ...state, firstName: action.payload };
    case 'lastName':
      return { ...state, lastName: action.payload };
    case 'age':
      return { ...state, age: action.payload };
    case 'employed':
      return { ...state, employed: action.payload };
    case 'color':
      return { ...state, color: action.payload };
    case 'sauce':
      return { ...state, sauce: action.payload };
    case 'stooge':
      return { ...state, stooge: action.payload };
    case 'notes':
      return { ...state, notes: action.payload};
    case 'all':
      return { ...initialState }
    default:
      throw new Error('wrong action');
  }
}

function errorReducer(state, action) {
  switch (action.type) {
    case 'firstName':
      return { ...state, firstName: action.payload };
    case 'lastName':
      return { ...state, lastName: action.payload };
    case 'age':
      return { ...state, age: action.payload };
    case 'notes':
      return { ...state, notes: action.payload};
    case 'all':
      return { ...initialErrors }
    default:
      throw new Error('wrong action in error reducer');
  }
}

const initialErrors = {
  firstName: "",
  lastName: "",
  age: "",
  notes: ""
}

export const User = () => {
  const [state, dispatcher] = useReducer(reducer, initialState);
  const [areButtonsBlocked, setIsButtonsBlocked] = useState(false);
  const [errors, errorDispatcher] = useReducer(errorReducer, initialErrors)

  useEffect(() => {
    if (
      !state.firstName &&
      !state.lastName &&
      !state.age &&
      !state.employed &&
      !state.color &&
      !state.sauce.ketchup &&
      !state.sauce.mustard &&
      !state.sauce.mayonnaise &&
      !state.sauce.guacamole &&
      state.stooge === 'Larry'
    ) {
      setIsButtonsBlocked(true);
    } else {
      setIsButtonsBlocked(false);
    }
  }, [
    state.firstName,
    state.lastName,
    state.age,
    state.employed,
    state.color,
    state.sauce,
    state.stooge
  ]);

  const reset = () => {
    // dispatcher({ type: 'firstName', payload: '' });
    // dispatcher({ type: 'lastName', payload: '' });
    // dispatcher({ type: 'age', payload: '' });
    // dispatcher({ type: 'employed', payload: false });
    // dispatcher({ type: 'color', payload: '' });
    // dispatcher({
    //   type: 'sauce',
    //   payload: {
    //     ketchup: false,
    //     mustard: false,
    //     mayonnaise: false,
    //     guacamole: false,
    //   },
    // });
    // dispatcher({ type: 'stooge', payload: 'Larry' });
    // dispatcher({type: 'notes', payload: ''})
    dispatcher({type: 'all'});
    errorDispatcher({type: 'all'});
    setIsButtonsBlocked(true);
  };

  const isValidFirstName = (input) => {
    const regex = /^[A-Za-zА-Яа-яЁё -]+$/;
    return !!input.match(regex);
  }

  const isValidLastName = (input) => {
    let regex = /^[A-Za-zА-Яа-яЁё -]+$/;
    return !!input.match(regex);
  }

  const isValidForAge = (input) => {
    return !isNaN(Number(input));
  }

  const isValidForNotes = (input) => {
    return input.length <= 100;
  }

  const submit = (e) => {
    e.preventDefault();
    if (!isValidFirstName(state.firstName)){
      errorDispatcher({type: 'firstName', payload: "wrong first name"});
      return;
    }
    if (!isValidLastName(state.lastName)){
      errorDispatcher({type: 'lastName', payload: "wrong last name"});
      return;
    }
    if (!isValidForAge(state.age)){
      errorDispatcher({type: 'age', payload: "wrong age"});
      return;
    }
    if (!isValidForNotes(state.notes)){
      errorDispatcher({type: 'notes', payload: "wrong notes"});
      return;
    }
    // errorDispatcher({type: 'all', payload: ""})
    alert(JSON.stringify(state));
    // errorDispatcher({type: 'firstName', payload: !isValidFirstName(state.firstName) ? "wrong first name" : ""});
    // errorDispatcher({type: 'lastName', payload: !isValidLastName(state.lastName) ? "wrong last name" : ""});
    // errorDispatcher({type: 'age', payload: !isValidForAge(state.age) ? "wrong age" : ""});
    // errorDispatcher({type: 'notes', payload: !isValidForNotes(state.notes) ? "wrong notes" : ""});
    // console.log(state.firstName);
    // console.log(state.lastName);
    // console.log(state.age);
    // console.log(state.employed);
    // console.log(state.color);
    // console.log(state.sauce);
    // console.log(state.stooge);
    // alert(JSON.stringify(state));
  };

  const handleColor = (event) => {
    dispatcher({ type: 'color', payload: event.target.value });
  };

  const handleSauce = (event) => {
    const target = event.target;
    const value = target.checked;
    const name = target.name;

    dispatcher({
      type: 'sauce',
      payload: {
        ...state.sauce,
        [name]: value,
      },
    });
  };

  const handleStooge = (event) => {
    dispatcher({ type: 'stooge', payload: event.target.value });
  };

  const handleNotes = (event) => {
    dispatcher({ type: 'notes', payload: event.target.value });
  }

  return (
    <div className="container">
      <div className="wrapper">
        <form className="user" onSubmit={submit}>
          <label htmlFor="name">First Name</label>
          <input
            value={state.firstName}
            type="text"
            name="name"
            placeholder="First Name"
            onChange={(e) => {
              dispatcher({ type: 'firstName', payload: e.target.value });
              errorDispatcher({type: 'firstName', payload: !isValidFirstName(state.firstName) ? "wrong first name" : ""})
            }}
            className={"field " + (errors.firstName ? 'field--error' : 'field--normal')}
          />
          <label htmlFor="surname">Last Name</label>
          <input
            value={state.lastName}
            type="text"
            name="surname"
            placeholder="Last Name"
            onChange={(e) => {
              dispatcher({ type: 'lastName', payload: e.target.value });
            }}
            className={"field " + (errors.lastName ? 'field--error' : 'field--normal')}
          />
          <label htmlFor="age">Age</label>
          <input
            value={state.age}
            type="text"
            name="age"
            placeholder="Age"
            onChange={(e) => {
              dispatcher({ type: 'age', payload: e.target.value });
            }}
            className={"field " + (errors.age ? 'field--error' : 'field--normal')}
          />
          <label htmlFor="employed">Employed</label>
          <input
            checked={state.employed}
            type="checkbox"
            name="employed"
            onChange={(e) => {
              dispatcher({ type: 'employed', payload: e.target.checked });
            }}
          />
          <label htmlFor="color">Favorite Color</label>
          <select name="color" value={state.color} onChange={handleColor} className='field--normal'>
            <option></option>
            <option value="white">White</option>
            <option value="black">Black</option>
            <option value="red">Red</option>
            <option value="green">Green</option>
          </select>
          <label htmlFor="sauces">Sauces</label>
          <div>
            <div>
              <input
                type="checkbox"
                name="ketchup"
                id="ketchup"
                checked={state.sauce.ketchup}
                onChange={handleSauce}
              />
              <label htmlFor="ketchup">Ketchup</label>
            </div>
            <div>
              <input
                type="checkbox"
                name="mustard"
                id="mustard"
                checked={state.sauce.mustard}
                onChange={handleSauce}
              />
              <label htmlFor="mustard">Mustard</label>
            </div>
            <div>
              <input
                type="checkbox"
                name="mayonnaise"
                id="mayonnaise"
                checked={state.sauce.mayonnaise}
                onChange={handleSauce}
              />
              <label htmlFor="mayonnaise">Mayonnaise</label>
            </div>
            <div>
              <input
                type="checkbox"
                name="guacamole"
                id="guacamole"
                checked={state.sauce.guacamole}
                onChange={handleSauce}
              />
              <label htmlFor="guacamole">Guacamole</label>
            </div>
          </div>
          <label htmlFor="stooge">Best Stooge</label>
          <div>
            <div>
              <input
                type="radio"
                id="Larry"
                name="stooge"
                value="Larry"
                checked={state.stooge === 'Larry'}
                defaultChecked
                onChange={handleStooge}
              />
              <label htmlFor="Larry">Larry</label>
            </div>
            <div>
              <input
                type="radio"
                id="Moe"
                name="stooge"
                value="Moe"
                checked={state.stooge === 'Moe'}
                onChange={handleStooge}
              />
              <label htmlFor="Moe">Moe</label>
            </div>
            <div>
              <input
                type="radio"
                id="Curly"
                name="stooge"
                value="Curly"
                checked={state.stooge === 'Curly'}
                onChange={handleStooge}
              />
              <label htmlFor="Curly">Curly</label>
            </div>
          </div>
          <label htmlFor="Notes">Notes</label>
          <textarea
            name="Notes"
            cols="30"
            rows="2"
            placeholder="Notes"
            value={state.notes}
            onChange={handleNotes}
            className={"field " + (errors.notes ? 'field--error' : 'field--normal')}
          ></textarea>
          <div className="spanned">
            <button
              className="primary"
              disabled={areButtonsBlocked}
              type="submit"
              onClick={submit}
            >
              Submit
            </button>
            <button
              id="resetButton"
              disabled={areButtonsBlocked}
              onClick={reset}
            >
              Reset
            </button>
          </div >
          <div className="summary spanned">
            {JSON.stringify(state, null, 4)}
          </div>
        </form>
      </div>
    </div>
  )
};

