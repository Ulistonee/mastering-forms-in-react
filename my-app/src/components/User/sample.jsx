import React from "react";
import { Formik } from "formik";

const LoginForm = () => {

  const handleSubmit = (values, errors) => {
    console.log("Success! Call the API Now!");
  };

  return (
    <div>
      <Formik
        initialValues={{
          email: "",
          password: "",
          acceptTOS: false,
        }}
        onSubmit={(values, errors) => {
          handleSubmit(values, errors);
        }}
      >
        {({ values, handleSubmit, handleChange }) => {
          return (
            <form onSubmit={handleSubmit}>
              <div>
                <input
                  value={values.email}
                  onChange={handleChange("email")}
                />
              </div>
              <div>
                <input
                  value={values.password}
                  onChange={handleChange("password")}
                />
              </div>
              <div>
                <input
                  type="checkbox"
                  checked={values.acceptTOS}
                  onChange={handleChange("acceptTOS")}
                />{" "}
                I accept TOS
              </div>
              <button>Submit</button>
            </form>
          );
        }}
      </Formik>
    </div>
  );
};

export default LoginForm;
